"""simple_votings URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from main import views
from django.contrib.auth import views as auth_views

from main.views import get_menu_context

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index_page, name='index'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('create_voting/', views.create_voting_page, name='create_voting'),
    path('registration/', views.registration_page, name='registration'),
    path('edit_voting/<int:_id>', views.edit_voting_page, name='edit_voting'),
    path('voting/<int:_id>', views.voting_page, name="voting"),
    path('profile/<int:_id>', views.profile_page, name="profile"),
    path('complaint/', views.complaint_page, name="complaint"),
    path('registration/', views.registration_page, name='registration'),
    path('all_votings/', views.all_votings, name='all_voting'),
    path('all_votings/<int:_id>', views.all_votings, name='all_voting'),
    path('login/', views.login_page, name='login')
]
