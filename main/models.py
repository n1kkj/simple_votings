from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    fullname = models.CharField(max_length=127)
    age = models.IntegerField(null=True)
    status = models.CharField(null="Я здесь недавно!", max_length=255)
    user = models.OneToOneField(to=User, on_delete=models.CASCADE)


class Voting(models.Model):
    voteTitle = models.TextField(max_length=1024)
    voteText = models.TextField(max_length=1024)
    votePicture = models.FileField(null=True)
    createTime = models.DateTimeField(null=True)
    voteIsPublic = models.BooleanField(default=False)
    user_id = models.IntegerField(null=True)
