import datetime

from django.shortcuts import render

from django.contrib.auth import authenticate
from django.contrib import auth
from django.contrib.auth import login
from django.contrib.auth.models import User


from .templates.votings.forms import VoteForm
from django.shortcuts import redirect

from django.contrib import auth
from django.contrib.auth import login
from django.contrib.auth.models import User
from main.forms import UserForm, LoginForm
from main.models import Profile, Voting

def get_menu_context():
    pass

def get_user_id(request):
    if auth.get_user(request).id:
        return int(auth.get_user(request).id)
    else:
        return 0

def index_page(request):
    context = {
        'pagename': 'Главная',
        'pages': 4,
    }
    context['user_id'] = get_user_id(request)
    return render(request, 'pages/index.html', context)


def time_page(request):
    context = {
        'pagename': 'Текущее время',
        'time': datetime.datetime.now().time(),
    }
    context['user_id'] = get_user_id(request)
    return render(request, 'pages/time.html', context)


def profile_page(request, _id):
    context = {}
    context['user_id'] = get_user_id(request)
    current_user = Profile.objects.get(id=_id)
    if request.method == "GET":
        context["fullname"] = current_user.fullname
        context["age"] = current_user.age
        context["status"] = current_user.status
        context["user"] = current_user.user
    return render(request, 'pages/profile.html', context)


def voting_page(request, _id):
    vote = Voting.objects.get(id=_id)
    context = {
        'title': vote.voteTitle,
        'description': vote.voteText,
        'question': {
            "name": vote.voteTitle,
            "ques": [
                {
                    "id": "1",
                    "txt": "Да (вариант ответа 1)",
                },
                {
                    "id": "2",
                    "txt": "Нет (вариант ответа 2)",
                },
                {
                    "id": "3",
                    "txt": "Затрудняюсь ответить (вариант ответа 3)",
                },
            ],
        },

    }
    return render(request, "pages/voting.html", context)


def create_voting_page(request):
    context = {}
    context['user_id'] = get_user_id(request)
    if request.method == "POST":
        form = VoteForm(request.POST)
        if form.is_valid():
            item = Voting(voteTitle=form.data["title"], voteText=form.data["text"])
            if auth.get_user(request).is_authenticated:
                item.user_id = int(auth.get_user(request).id)
            else:
                item.user_id = 0
            item.save()
            context["title"] = form.data["title"]
            context["text"] = form.data["text"]
            context["form"] = form
            redirect('all_votings/' + str(item.user_id))
        else:
            context["form"] = form
    else:
        context["form"] = VoteForm()
    return render(request, 'pages/create_voting.html', context)


def edit_voting_page(request, _id):
    context = {}
    context['user_id'] = get_user_id(request)
    if request.method == "POST":
        form = VoteForm(request.POST)
        if form.is_valid():
            current_voting = Voting.objects.get(id=_id)
            current_voting.voteTitle = form.data["title"]
            current_voting.voteText = form.data['text']
            current_voting.save()
            return redirect('/voting/' + str(_id))
        else:
            context["form"] = form
    else:
        current_voting = Voting.objects.get(id=_id)
        context["title"] = current_voting.voteTitle
        context["text"] = current_voting.voteText
        context["form"] = VoteForm()
    return render(request, 'pages/edit_voting.html', context)


def complaint_page(request):  # Страница жалоб, а не доносов, как в оригинале :/
    context = {}
    context['user_id'] = get_user_id(request)
    return render(request, 'pages/complaint.html', context)


def registration_page(request):
    context = {}
    context['user_id'] = get_user_id(request)
    context['pagename'] = 'Регистрация'
    context['error'] = ''
    if auth.get_user(request).is_authenticated:
        return redirect('/')
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            if form.data["password"] == form.data["sec_password"]:
                user1 = User.objects.filter(username=form.data["name"])
                if not user1:
                    user = User(username=form.data["name"])  # Другое надо поставить, но username обязателен
                    user.set_password(form.data['password'])
                    user.save()
                    new_profile = Profile(fullname=form.data["name"], status=form.data['password'], age=25, user=user)  # Некоторые поля я сделал для себя null
                    new_profile.save()
                    return redirect('/profile/' + str(new_profile.user.id))
                else:
                    context["error"] = "Username is not unique!"
        context["form"] = form
    else:
        context['form'] = UserForm(request.POST)
    return render(request, 'pages/registration.html', context)


def all_votings(request, _id=0):
    if _id == 0:
        context = {
            'pagename': 'Все голосования',
            'votings': Voting.objects.all(),
        }
    else:
        context = {
            'pagename': 'Все голосования',
            'votings': Voting.objects.filter(user_id=_id),
        }
    context['user_id'] = get_user_id(request)
    return render(request, 'pages/all_votings.html', context)

def login_page(request):
    context = {}
    context['user_id'] = get_user_id(request)
    context["pagename"] = 'Авторизация'
    if request.method == "GET":
        form = LoginForm()
        context['form'] = form
        if auth.get_user(request).is_authenticated:
            return redirect('/')

    if request.method == "POST":
        if auth.get_user(request).is_authenticated:
            return redirect('/')
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=form.data["username"], password=form.data["password"])
            login(request, user)
            return redirect('/')
        else:
            return redirect('/login')

    return render(request, "pages/login.html", context)

def logout_page(request):
    auth.logout(request)
    return redirect('')
