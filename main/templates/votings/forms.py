from django import forms

from main.models import Voting


class VoteForm(forms.Form):
    attrs = {}
    title = forms.CharField(required=True, label="title", widget=forms.TextInput(attrs=attrs))
    text = forms.CharField(required=True, label="text", widget=forms.Textarea(attrs=attrs))
