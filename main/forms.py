from django import forms

class UserForm(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    name = forms.CharField(label="Имя", required=True, widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(label="Пароль", min_length=8, required=True, widget=forms.PasswordInput(attrs=attrs))
    sec_password = forms.CharField(label="Повторите пароль", required=True, widget=forms.PasswordInput(attrs=attrs))

class LoginForm(forms.Form):
    """
    Форма входа

    :param username: получаем логин/юзернейм (CharField)
    :param password: получаем пароль (CharField)
    """
    username = forms.CharField(label="Имя пользователя", required=True)
    password = forms.CharField(required=True)
